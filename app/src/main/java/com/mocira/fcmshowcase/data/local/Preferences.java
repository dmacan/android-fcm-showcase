package com.mocira.fcmshowcase.data.local;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by davidmacan on 14/02/2017.
 */

public class Preferences {

    private SharedPreferences sp;

    public Preferences(Context context) {
        sp = context.getSharedPreferences("com.mocira.fcmshowcase", Context.MODE_PRIVATE);
    }

    public Boolean storeToken(String token) {
        return sp.edit().putString("token", token).commit();
    }

    public String getToken() {
        return sp.getString("token", null);
    }

}

package com.mocira.fcmshowcase.presentation.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mocira.fcmshowcase.R;
import com.mocira.fcmshowcase.data.local.Preferences;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String token = new Preferences(this).getToken();
        Log.i("Mocira", "Token: " + token);
    }
}
